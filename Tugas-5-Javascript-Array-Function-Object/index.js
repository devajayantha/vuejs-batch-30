/** Soal 1 */
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (var n = 0; n < daftarHewan.sort().length; n++) {
    console.log(daftarHewan[n]);
}

/** Soal 2 */
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

function introduce(data) {
    return 'Nama saya '+ data.name +', umur saya '+ 
    data.age + ' tahun, alamat saya di '+ 
    data.address + ', dan saya punya hobby '+ data.hobby;
}

var perkenalan = introduce(data);

console.log(perkenalan);

/** Soal 3 */
function hitung_huruf_vokal (input)
{
    return input.match(/[aeiou]/gi).length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2);

/** Soal 4 */
function hitung(number)
{
    if (number <= 0) {
        return (number + -1) * 2;
    }

    return (number - 1) * 2;
}

console.log(hitung(4));