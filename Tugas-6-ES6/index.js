/** Soal 1 */
let p = 20
let l = 25

const around = (p, l) => {
    return 2 * (p + l) 
}

const broad = (p, l) => {
    return p * l 
}

console.log("Keliling : "+ around(p, l))
console.log("Luas : "+ broad(p, l))

/** Soal 2 */
const literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: firstName +' '+ lastName
    }
}

console.log(literal("William", "Imoh").fullName)

/** Soal 3 */
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

console.log(newObject.firstName, newObject.lastName, newObject.address, newObject.hobby)

/** Soal 4 */
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)

/** Soal 5 */
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}` 

console.log(before)