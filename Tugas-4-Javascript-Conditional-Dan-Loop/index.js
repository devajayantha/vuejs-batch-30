/** Soal 1 */
var nilai = 80;
var index = '';

if (nilai == 85) {
    text = 'A';
} else if (nilai >= 75 && nilai < 85) {
    text = 'B';
} else if (nilai >= 65 && nilai < 75) {
    text = 'C';
} else if (nilai >= 55 && nilai < 65) {
    text = 'D';
} else {
    text = 'E';
}

console.log('indeksnya : ' + String(text));

/** Soal 2 */
var tanggal = 11;
var bulan = 3;
var tahun = 1998;

switch(bulan) {
    case 1:   { console.log('11 Januari 1998'); break; }
    case 2:   { console.log('11 Februari 1998'); break; }
    case 3:   { console.log('11 Maret 1998'); break; }
    case 4:   { console.log('11 April 1998'); break; }
    case 5:   { console.log('11 Mei 1998'); break; }
    case 6:   { console.log('11 Juni 1998'); break; }
    case 7:   { console.log('11 Juli 1998'); break; }
    case 8:   { console.log('11 Agustus 1998'); break; }
    case 9:   { console.log('11 September 1998'); break; }
    case 10:   { console.log('11 Oktober 1998'); break; }
    case 11:   { console.log('11 November 1998'); break; }
    case 12:   { console.log('11 Desember 1998'); break; }
    default:  { console.log('not found'); }
}

/** Soal 3 */
var n = 3;
var value = '';

for(var a = 0; a < n; a++) {
    for(var b = 0; b <= a; b++) {
        value += '*';
    } 
    value += '\n';
} 

console.log(value);

var x = 7;
var valueDua = '';

for(var a = 0; a < x; a++) {
    for(var b = 0; b <= a; b++) {
        valueDua += '*';
    } 
    valueDua += '\n';
} 

console.log(valueDua);

/** Soal 4 */


/**
 * not complicated
 */
var n = 3;
var text = ['programming', 'Javascript', 'VueJS'];

for (var a = 0; a < n; a++) {
    console.log('I Love '+ text[a]);
}
